<?php

namespace animals;

/**
 * Class Animal
 * @package animals
 */
abstract class Animal
{
    /**
     * @var string
     */
    private $name;

    /**
     * Animal constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
