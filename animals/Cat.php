<?php

namespace animals;

/**
 * Class Cat
 * @package animals
 */
class Cat extends Animal
{
    /**
     * @return string
     */
    public function meow()
    {
        return "Cat {$this->getName()} is saying meow";
    }
}
