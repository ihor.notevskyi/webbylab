<?php

use components\Autoload;
use animals\Cat;

require_once __DIR__ . '/components/Autoload.php';

spl_autoload_register([
    new Autoload(__DIR__),
    'load'
]);

$cat = new Cat ('garfield');

var_dump($cat->getName() === 'garfield');
var_dump($cat->meow() === 'Cat garfield is saying meow');
